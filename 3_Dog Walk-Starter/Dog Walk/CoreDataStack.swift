//
//  CoreDataStack.swift
//  Dog Walk
//
//  Created by Jeff Ma on 2/26/15.
//  Copyright (c) 2015 Razeware. All rights reserved.
//

import CoreData

class CoreDataStack {
    let context: NSManagedObjectContext
    let psc: NSPersistentStoreCoordinator
    let model: NSManagedObjectModel
    let store: NSPersistentStore?

    init() {
        // load the managed object model from disk
        let bundle = NSBundle.mainBundle()
        let modelURL = bundle.URLForResource("Dog Walk", withExtension: "momd")
        model = NSManagedObjectModel(contentsOfURL: modelURL!)!

        psc = NSPersistentStoreCoordinator(managedObjectModel: model)

        context = NSManagedObjectContext()
        context.persistentStoreCoordinator = psc

        //let documentsURL = applicationDocumentsDirectory()

        let fileManager = NSFileManager.defaultManager()
        let urls = fileManager.URLsForDirectory(.DocumentDirectory,
            inDomains: .UserDomainMask) as! [NSURL]
        let documentsURL = urls[0]

        let storeURL = documentsURL.URLByAppendingPathComponent("Dog Walk")
        let options = [NSMigratePersistentStoresAutomaticallyOption:true]

        var error: NSError?
        store = psc.addPersistentStoreWithType(NSSQLiteStoreType,
            configuration: nil,
            URL: storeURL,
            options: options,
            error: &error)

        if store == nil {
            println("Error adding persistent store: \(error)")
            abort()
        }

    }

    func applicationDocumentsDirectory() -> NSURL {
        let fileManager = NSFileManager.defaultManager()
        let urls = fileManager.URLsForDirectory(.DocumentDirectory,
            inDomains: .UserDomainMask) as! [NSURL]
        return urls[0]
    }

    func saveContext() {
        var error: NSError?
        if context.hasChanges && !context.save(&error) {
            println("Counld not save: \(error), \(error?.userInfo)")
        }
    }


}





















