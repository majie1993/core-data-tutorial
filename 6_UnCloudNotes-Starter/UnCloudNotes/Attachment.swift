//
//  Attachment.swift
//  UnCloudNotes
//
//  Created by Jeff Ma on 3/4/15.
//  Copyright (c) 2015 Ray Wenderlich. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class Attachment: NSManagedObject {
    @NSManaged var dateCreated: NSDate
    @NSManaged var image: UIImage?
    @NSManaged var note: Note
}

