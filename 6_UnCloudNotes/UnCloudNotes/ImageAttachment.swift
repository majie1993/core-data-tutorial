//
//  ImageAttachment.swift
//  UnCloudNotes
//
//  Created by Jeff Ma on 3/4/15.
//  Copyright (c) 2015 Ray Wenderlich. All rights reserved.
//

import UIKit
import CoreData

class ImageAttachment: Attachment {
    @NSManaged var image: UIImage?
    @NSManaged var width: CGFloat
    @NSManaged var height: CGFloat
    @NSManaged var caption: NSString
}
