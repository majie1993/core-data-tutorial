//
//  AttachmentToImageAttachmentMigrationPolicyV3toV4.swift
//  UnCloudNotes
//
//  Created by Jeff Ma on 3/4/15.
//  Copyright (c) 2015 Ray Wenderlich. All rights reserved.
//

import CoreData
import UIKit

class AttachmentToImageAttachmentMigrationPolicyV3toV4: NSEntityMigrationPolicy {
    override func createDestinationInstancesForSourceInstance(sInstance: NSManagedObject, entityMapping mapping: NSEntityMapping, manager: NSMigrationManager, error: NSErrorPointer) -> Bool {

        // 有两个context，一个负责读原始的，一个负责写入新的，所以这里应该是从写入的新context中搞一个attachment
        let newAttachment = NSEntityDescription.insertNewObjectForEntityForName("ImageAttachment", inManagedObjectContext: manager.destinationContext) as! NSManagedObject

        // 把默认的for一下，让他们执行
        for propertyMapping in mapping.attributeMappings as! [NSPropertyMapping] {
            let destinationName = propertyMapping.name!
            if let valueExpression = propertyMapping.valueExpression {
                let context: NSMutableDictionary = ["source" : sInstance]
                let destinationValue: AnyObject = valueExpression.expressionValueWithObject(sInstance, context: context)
                newAttachment.setValue(destinationValue, forKey: destinationName)
            }
        }

        // 尝试从原始中拿一个image出来
        if let image = sInstance.valueForKey("image") as? UIImage {
            newAttachment.setValue(image.size.width, forKey: "width")
            newAttachment.setValue(image.size.height, forKey: "height")
        }

        // caption - 标题
        let body = sInstance.valueForKeyPath("note.body") as! NSString
        newAttachment.setValue(body.substringFromIndex(80), forKey: "caption")

        // migration manager 需要知道从原始到目标的链路
        manager.associateSourceInstance(sInstance, withDestinationInstance: newAttachment, forEntityMapping: mapping)

        return true
    }
}











